anonimal   Hi all. Welcome to the first annual #kovri-meta meeting.
anonimal   1. Greetings
anonimal   2. Statements
anonimal   3. Q&A
anonimal   4. Schedule next meeting
xmrmatterbridge   <rehrar> Everyone stand for the Kovri Anthem
*   anonimal wonders how that goes
ErCiccione   greetings time? :P
anonimal   1. Greetings
xmrmatterbridge   <rehrar> Oh kooooovri, our baaaaaaby, we hold in you in our aaaaaarms.
xmrmatterbridge   <rehrar>
xmrmatterbridge   <rehrar> Oh kooooovri, the beeeeestesty, we love you in our heaaaaarts.
xmrmatterbridge   <rehrar> Thank you.
asymptotically   greetings, internet friends
ErCiccione   Hi
luuuke   Hola
xmrmatterbridge   <rehrar> Hi
kovri-slack   <mermaidnets> Hello
anonimal   Hello to all, welcome
anonimal   2. Statements
anonimal   Alright, let's get this ball rolling
anonimal   So there may be some confusion or questions about recent project developments.
anonimal   I'd like rehrar to explain what's going on. Diego, spotlight.
xmrmatterbridge   <rehrar> Ok so.
xmrmatterbridge   <rehrar> Here's the deal. Kovri has been setting up a lot of its own infrastructure recently. And some people are confused because they think that means Kovri is trying to separate itself from Monero.
xmrmatterbridge   <rehrar> There were more than a few hiccups with the alpha release in regards to Kovri's reliance on the infrastructure provided by the Monero Project, and, rather than continue to use it and foster I'll will between both projects, anonimal has decided it's best to have a separate infrastructure.
xmrmatterbridge   <rehrar> *ill
xmrmatterbridge   <rehrar> This also goes along with both Monero's and Kovri's vision of decentralization and minimizing single points of failure, so this move furthers that end as well.
xmrmatterbridge   <rehrar> All of this is ultimately leading towards a bit of a shift in the Kovri project that I will do my best to summarize.
xmrmatterbridge   <rehrar> Kovri owes a lot to Monero and the people in it. Without the Monero Project, Kovri probably wouldn't exist, or would at least very differently than it does today. But as anyone who sticks around here for any length of time knows, Kovri is not meant to be for Monero, but rather for everyone.
xmrmatterbridge   <rehrar> Of course Monero can (and will) use Kovri, but the goal is for anyone and everyone who wants to use Kovri for personal or application use can do so.
xmrmatterbridge   <rehrar> Those who were at Defcon may recall Sean speaking about many many exciting usecase for something like Kovri. It has a lot of potential, and we'd like to see it grow to that potential.
xmrmatterbridge   <rehrar> So the analogy to think of is that when Kovri was taken under the wing of Monero, it was a small child, shivering, cold, and alone. But children grow, and at some point they got to get off of their parent's medical insurance and get their own.
xmrmatterbridge   <rehrar> The goal is for Kovri to come into its own as a project. The parent/child analogy is very appropriate here, because even after a child grows up, there is usually still great love, respect, and relationships with the parents. And Kovri wishes to be still connected in development and relationship with the Monero Project, while still making decisions for the software that may not in <message clipped>
xmrmatterbridge   <rehrar> This transition is not an all or nothing and overnight thing though. It's not as if yesterday we were tight and tomorrow we're separate.
xmrmatterbridge   <rehrar> It will be a process and will take time.
xmrmatterbridge   <rehrar> It's important to note that this has always been the goal for Kovri. To be used by all, and not just by Monero. It may just be happening on a slightly faster timeline than anticipated so as not to overburden both Kovri and Monero with unnecessary joint responsibility.
xmrmatterbridge   <rehrar> We also believe this move will be healthy for the Monero Project in more than just decentralization, as it allows maintainers of infrastructure for Monero things (pigeons, myself, and pony) to focus on those Monero things and do them well.
xmrmatterbridge   <rehrar> Since Monero (and even more so Kovri) is short of volunteers on these things, it's important to keep things manageable. I for one work with pigeons in maintaining a number of things, and see how he runs ragged fixing a lot of things and adding more (many times at my request), and think this move will help alleviate some of that work and put it on newer, fresher, shoulders.
xmrmatterbridge   <rehrar> Like it or not, a lot of OSS is about producing quality stuff while not burning out our contributors, and there have been some signs of burning out on many fronts for a while now. We hope this move can help bring some new and refreshing energy into both projects.
xmrmatterbridge   <rehrar> So in summary:
Corklander   Hello
xmrmatterbridge   <rehrar> Kovri is not leaving, but things are indeed changing a bit. The relationship is becoming a bit more defined, and is actually moving toward the original vision for Kovri.
xmrmatterbridge   <rehrar> I hope this cleared some stuff up for anyone with questions.
xmrmatterbridge   <rehrar> Anything I missed?
moneromooo   That sounds awfully like one of those breakup speeches where you really want to soften the blow to the other person as much as you can :)
anonimal   lolololol
ErCiccione   lol
anonimal   Nails it on the head
xmrmatterbridge   <rehrar> It's not you, it's me.
luuuke   It's not you, it's me.
*   Corklander is missing the channel history
luuuke   lol
xmrmatterbridge   <rehrar> Corklander, slack or mattermost to catch up.
anonimal   sean I'll end up pasting the meeting at the end
anonimal   So, rehrar I need to clarify a few false points but for the most part that is correct:
xmrmatterbridge   <rehrar> Mattermost has the backlog.
Corklander   kk
xmrmatterbridge   <rehrar> Well, I tried :P
anonimal   You did great, thanks
kovri-slack   <mermaidnets> Do you have a lose timeline for the changes?
anonimal   We'll take Q&A in the next section mermaidnets
anonimal   So, some clarifications:
anonimal   I've sat here for 3 years, watched the monero community explode, and am still watching 99.5% of the community not contribute to kovri.
anonimal   I can count on two hands... TWO. You know who you are.
anonimal   This is inexcusable and plain-as-day fact that kovri is *not* part of monero outside of the contributors who are funded in monero.
anonimal   So few people understand wtf a transport layer is and don't realize that without a transport layer you don't have a functional cryptocurrency ecosystem.
anonimal   And if that transport layer is not anonymized or secured, then there you go.
anonimal   It's like, the community cares more about the face, chest, arms, and torso of monero but not the legs.
anonimal   Like, imagine Arnold Schwarzenegger with scrawny chicken legs.
anonimal   That's the monero community's interest right now. A hybrid Arnold.
anonimal   Expecting me a and handful of people to do the rest is ridiculous.
anonimal   occassion menti
anonimal   on at conferences up until this summer, almost no outreach resources diverted to kovri, etc. etc. etc. etc.
anonimal   *occassional mention
anonimal   I mean I can really go on and on and on. It's not like any of this is a surprise.
anonimal   We know who is involved. We know who is not. It's plain as day and for this project to survive we need do what I should've done from the beginning and not taken bad advice.
anonimal   So, more to add:
anonimal   I really hate having to use the 3rd person analogy because I don't speak in the 3rd person
anonimal   but there is a "monero project anonimal" and "kovri project anonimal".
anonimal   Both want the success of monero and kovri. Period. Nothing has changed there.
anonimal   But for monero to do better with kovri, kovri needs to really be it's own project, not play favorites, BE application agnostic, ALLOW non-monero contributors (people who don't like monero but like the idea of kovri), and find alternate revenue.
anonimal   My FFS doesn't change. The goals of integration with monero first doesn't change.
anonimal   Monero project anonimal wants this happen and kovri project anonimal wants this to happen.
anonimal   My point: when we start acting like our own project, I don't want to hear a bunch of bitching and moaning. The folks not here had 3 years to do something of value for this project. If they complain, I don't care.
anonimal   So, ultimately, and I'm reallly reallly really glad to make this point:
anonimal   The code goals do not change. The code is in the end what matters. Any actions I take are in the best interest of anyone even if they don't understand why.
anonimal   I think rehrar summed that up for the most part.
anonimal   Do I take any of this personally? Well, not really.
anonimal   99% no, because everyone here is free to do what they want.
anonimal   I do take issue with one person though simply because I don't like being lied to or strung along.
anonimal   But aside from that, really this is all good for everyone.
anonimal   Does that make sense?
oneiric_   lied to?
anonimal   Yeah, I can open the can but it's not worth now because it's between him and I.
oneiric_   alright
moneromooo   So a little innuendo will have to suffice :)
xmrmatterbridge   <rehrar> wined and dined but no ring on the finger.
anonimal   He's not even here, so I won't talk behind his back.
anonimal   rehrar more like loaned and redacted
anonimal   lol
anonimal   Anyway, what's done is done. It's time to move forward.
anonimal   Shall we move onto Q&A?
*   anonimal thinking of what else needs to be said
anonimal   Oh,
anonimal   Ah, nevermind. I'll end up talking about physics.
anonimal   Ok, so mermaidnets asked a question:
oneiric_   lol
anonimal   :)
anonimal   <mermaidnets> Do you have a lose timeline for the changes?
anonimal   That's a great question, but first I think we need to establish what needs to be changed and why.
anonimal   We don't have a formal document, I wish we did.
anonimal   It's extremely easy to put up a 'donation page' and say 'gimmie stuff' so I want to avoid that and focus more on quality revenue.
kovri-slack   <mermaidnets> Noted
anonimal   What I mean is, target those who benefit from kovri, build a relationship, etc.
anonimal   We should have that meeting/discussion soon'ish within the month I think.
anonimal   rehrar has ideas as well. There are also really important points I want to make about that because we do need to remain as decentralized as possible.
anonimal   I'll have to do my homework to give an answer to mermaidnets
anonimal   I also need to finish my FFS. So, ornate, ideal backend build systems may have to wait until that's done.
asymptotically   sorry for being out of the loop but do you have any more info on the GitHub issue where you said that you wanted to fork from the Java I2P network?
anonimal   I'm really needing to get back to coding. I'm sure oneiric_ is being patient.
anonimal   asymptotically: Hi. Ah, yes, that issue I would hope we would discuss next Thursday
anonimal   asymptotically: if you go onto gitlab.com/kovri-project/kovri it's in the issue tracker
anonimal   sidenote, re: backend, we should have kovri.i2p up very soon again, finally. I'll drop a line when that happens.
oneiric_   woot!
anonimal   oneiric_: you'll probably like this, but I would like to focus on code development on a single build system for the time being. We'll save so much freaking time. We can talk more next week
oneiric_   if we do fork away, can we get rid of AES ECB/CBC?
anonimal   We can do *a lot*
oneiric_   :)
oneiric_   I do like one build system, definitely timesaver
anonimal   AND, according to my contacts, it shouldn't be a problem because kovri has a realistic potential of outnumbering java I2P routers.
anonimal   TBD
kovri-slack   <sean> Yes, I suspect it does.
anonimal   But really, and I mean let's *really* think about that question of why there must be any relationship (the strongest argument was to "hide" within the number of routers). Anyway, next week let's talk more I hope, please.
anonimal   Awesome
anonimal   Ok, continuing on 3. Q&A:
anonimal   Does everything sense so far?
anonimal   *make sense
anonimal   I'm very proud of all the contributors to kovri. I really am. I know you are. I know your work. I know what you can do. I'm very impressed and glad to work with you all.
rafaeltokyo   do it for me
oneiric_   <3
anonimal   I really love working with monero too. I really really do. Monero project anonimal is very happy with monero outside of kovri.
anonimal   We'll do it for you rafaeltokyo
asymptotically   what should somebody looking to contribute do? check on the GitLab issues page until they find something that they feel comfortable doing?
anonimal   Yes. But we really need a solid set milestones, a wiki, something that is *not* moneropedia (i.e., something that can be wiki'd), and more
anonimal   A clear developer guide would be nice. We have one but maybe it's not as clear as it could be?
anonimal   I don't know, something we should definitely talk about at the dev meeting next Thursday, same time as today.
oneiric_   I can start chipping in on that once we have a general direction/framework set up
luuuke   ditto
anonimal   asymptotically: also you can ask us code questions. If I'm not around, oneiric_ is becoming more intimate with the code and can help
anonimal   sean is busy but also doing research, he can probably answer questions too.
anonimal   luuuke is a party animal so I don't know what he's up to
anonimal   JUST KIDDING ;)
anonimal   Before I forget, I'd like to make a formal introduction to two new contributors.
anonimal   I've introduced luuuke in #kovri-dev, in case anyone missed that. He has many skills so I don't want to define him as backend/devops but that's what he's helping us with right now.
rafaeltokyo   sorry i messed up anonimal "it is clear to me"
anonimal   And mermaidnets who is now onboard with our public relations and outreach. She has a strong background in business development, marketing and sales. Diego and I are working with her, bringing her up to speed.
anonimal   So, welcome mermaidnets :)
xmrmatterbridge   <rehrar> Yay new peeps.
oneiric_   welcome to the team mermaidnets!
anonimal   (she's working right now, probably won't see this message until later)
anonimal   Oh, one last thing. A meeting bot!
oneiric_   luuuke you kick ass, and you know this
anonimal   ^
luuuke   Aww shucks
anonimal   I will get the meeting bot going so we can use it. Also, a mailing list will be good too. I think that will come after our @kovri.io mailing needs are met.
anonimal   With 4 minutes left, question to all:
ErCiccione   i haven't much to say beside that i would like to help with more than the translations. Will find something i can be useful for
anonimal   What's it called, on websites that have their 'teams' listed and they all have headshots and titles. Should we do that?
anonimal   ErCiccione: YOU HAVE BEEN IMMENSELY USEFUL
xmrmatterbridge   <rehrar> oneiric with his mask, hat, and glasses.
oneiric_   picture of cthulhu por moi
oneiric_   :) rehrar
luuuke   techbro-circle-heads is think is the technical term
anonimal   The translation team is EXACTLY what kovri needs and EXACTLY what I was hoping to happen for the rest of the project from the monero community. THANK YOU and we need to get you more funding for kovri related work.
oneiric_   lol
anonimal   Ah, techbro-circle-heads that's it. Should we? Too cheesy?
*   oneiric_ votes no
kovri-slack   <sean> I'd need more goat photos then.
anonimal   ^^^^^
anonimal   lol yessss
anonimal   I mean, we could use avatars if so desired
luuuke   What about we do it, but everyone is wearing Balaclavas?
xmrmatterbridge   <rehrar> Alright. I gotta split. Cya.
oneiric_   bye rehrar o/
kovri-slack   <sean> See ya soon rehrar
anonimal   lol alright alright. We can keep it open ended for now?
anonimal   Bye rehrar thanks for stopping by
oneiric_   +1
anonimal   4. Schedule next meeting
ErCiccione   thanks anonimal, will keep doing my best. Hopefully I can create a kovri-dedicated team soon
anonimal   2 weeks? Same time?
luuuke   Put a pin in it, I think it's probably a good thing.
anonimal   Thanks ErCiccione
anonimal   Do we need more than every 2 weeks?
oneiric_   alternating with dev meetings?
ErCiccione   2 weeks same time sounds ok to me
oneiric_   same
anonimal   ok. Alternating could be good so we can focus on preparing for one and not both?
luuuke   good with me
kovri-slack   <sean> Good for me
anonimal   Awesome, ok. I'll make a note.
anonimal   Thanks everyone. Really, thanks :)
